%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




\renewcommand{\theequation}{\arabic{equation}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{fashionfuchsia} Laboratory IV  }\\[2mm]
{  \Large \textbf{\textit{\color{blue-violet} Introduction to Uncertainty}}  }\\[2mm]
{  \large \it \color{deeppink} Modern Physics --- Physics 303  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


\setlength{\fboxsep}{2mm}
\sisetup{separate-uncertainty = true, multi-part-units = single}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	In experimental work you are often presented with the following situation:
	a theory you want to test in laboratory makes predictions about some quantity \cc{ Q }.
	This quantity cannot be measured directly, but instead its value is obtained by
	measuring one or more related quantities \cc{ x }, \cc{ y }, \cc{ z } \emph{etc}.
	You know the relation \cc{ Q ~=~ Q(x,~ y,~ z,~ \dots) } and thus can calculate a value for \cc{ Q }
	from your measurements of \cc{ x }, \cc{ y }, \cc{ z }, \emph{etc}.
	But what is the uncertainty \cc{ \Delta Q } that results from the uncertainties
	\cc{ \Delta x }, \cc{ \Delta y }, \cc{ \Delta z }, \emph{etc} ?

\bigskip\noindent
	For example, consider an object released from rest near the Earth's surface.
	We will show in class that if you know that at time \cc{ t }, the distance \cc{ x } has fallen since its release
	is given by \cc{ x(t) ~=~ \frac 1 2\, g\, t^2 }, where \cc{ g } is the acceleration due to gravity.
	To find \cc{ g }, you can measure \cc{ x } and \cc{ t } and substitute their values into the equation,
	and find \cc{ g ~=~ 2\, x \,/\, t^2 }, obtained from the original equation for \cc{ x(t) }.
	Suppose you know the uncertainties \cc{ \Delta x } and \cc{ \Delta t }.
	What should you state for the uncertainty \cc{ \Delta g } in the calculated value for \cc{ g }?
	By the end of this section of the laboratory, you will be able to answer this question


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%          F U N C T I O N S   O F   A   S I N G L E   V A R I A B L E         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcounter{activity}
\refstepcounter{activity}
\section*{\textit{Functions of a Single Variable}}

	Suppose a quantity \cc{ Q } depends only on a single variable \cc{ x },
	\emph{i.e.} \cc{ Q ~=~ Q(x) }.
	For example, \cc{ Q } might be the area of a square with sides \cc{ x }, so that \cc{ Q ~=~ x^2 }.
	Let the uncertainty in \cc{ x } be \cc{ \Delta x }.
	The relevant question is: if the ``true'' value of \cc{ x } can be thought of as lying within \cc{ x ~\pm~ \Delta x },
	what is the corresponding range \cc{ Q ~\pm~ \Delta Q } about the calculated value of \cc{ Q }
	within which you expect to find the ``true'' value of \c{ Q }
	(\emph{i.e.} the value of \cc{ Q } calculated from the ``true'' value of \cc{ x })?

\bigskip\noindent
	To proceed, consider a particular case.
	Suppose the ``true'' value of \cc{ x } is really \cc{ x ~+~ \Delta x },
	\emph{i.e.} it is about as far away from the stated value \cc{ x } as it is likely to be.
	In this case, the ``true'' value of \cc{ Q } is not the stated value \cc{ Q(x) } but \cc{ Q(x \,+\, \Delta x) },
	and this value represents the farthest that the ``true'' value of \cc{ Q } is likely to be away from its stated value.
	Thus the uncertainty \cc{ \Delta Q } in the calculated value \cc{ Q } due to the uncertainty in \cc{ x }
	is just the magnitude of the difference between the extreme value \cc{ Q(x \,+\, \Delta x) }
	and the ``best guess'' value \cc{ Q(x) } ---
	that is, \cc{ \Delta Q ~=~ Q(x \,+\, \Delta x) ~-~ Q(x) }.
	But how does one calculate this difference?
	You can answer this question using differential calculus.
	Let \cc{ \Delta Q ~=~ Q(x \,+\, \Delta x) ~-~ Q(x) } and consider the ratio
\ccc
\beq
	\frac{\Delta Q}{\Delta x}	~~=~~	\frac{ Q(x \,+\, \Delta x) ~-~ Q(x) }{ \Delta x }
	\ccb\,.
	\ccc
\eeq
\ccb
	Here a \cc{ \Delta } quantity can be either positive or negative, depending on whether \cc{ Q(x \,+\, \Delta x) }
	is greater than or less than \cc{ Q(x) }, or \cc{ \Delta x } is positive or negative.
	As \cc{ \Delta x ~\to~ 0 }, the right hand side of the above equation approaches \cc{ dQ(x)/dx },
	and consecutively,
\ccc
\beq
	\Delta Q	~~\simeq~~	\left|\, \frac{ d\,Q }{ d\,x } \,\right|\, \Delta x
	\ccb\,.
	\ccc
\eeq
\ccb
\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	For the area example, where \cc{ Q ~=~ x^2 },
	suppose \cc{ x ~=~ \SI{10 \pm 1}\cm } (a \cc{ 10\% } uncertainty).
	Show that \cc{ Q ~=~ \SI{100 \pm 20}{\square\cm} } and find the percent uncertainty in \cc{ Q }
\vspace{4cm}
\end{tcolorbox}

\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	Suppose the equation for \cc{ Q(x) } is given by \cc{ k\, x^\alpha },
	where \cc{ k } and \cc{ \alpha } are constants that are known exactly.
	Show that the relative uncertainty in \cc{ Q } is \cc{ |\alpha| } times the relative uncertainty in \cc{ x }, \emph{i.e.}
\ccc
\[
	\frac{\Delta Q}{ Q }	~~=~~	| \alpha |\, \frac{ \Delta x }{ |x| }
	\ccb\,.
\]
\ccb
\vspace{4cm}
\end{tcolorbox}

\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	Again consider the area example where \cc{ Q ~=~ x^2 } and \cc{ x ~=~ \SI{10 \pm 1}\cm }.
	Now first find the relative uncertainty in \cc{ Q } by using the special case directly above.
	Then use your results for the relative uncertainty in \cc{ Q } and the value of \cc{ Q } to find \cc{ \Delta Q }.
	Your final result should agree with what you found before
\vspace{4cm}
\end{tcolorbox}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%          F U N C T I O N S   O F   S E V E R A L   V A R I A B L E S         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\refstepcounter{activity}
\section*{\textit{Functions of Several Variables}}

	Suppose \cc{ Q } is a function of several variables, \emph{i.e.} \cc{ Q ~=~ Q(x,~ y,~ z,~ \dots) }.
	Now you need to find the uncertainty \cc{ \Delta Q } that results from the uncertainties
	\cc{ \Delta x }, \cc{ \Delta y }, \cc{ \Delta z }, \emph{etc}.
	The generalization is not difficult to describe and is quite reasonable,
	but we will not give a detailed proof here

\begin{enumerate}[label=(\arabic*)]
\item
	First find the uncertainties in \cc{ Q } due to each variable \cc{ x }, \cc{ y }, \cc{ z } \emph{etc}
	separately using the method previously described, but treating the other variables as if they were constants
	when taking the derivative.
	That is, compute \cc{ \delta Q_x ~=~ | dQ/dx |\, \Delta x } holding all variables but \cc{ x } constant,
	\cc{ \delta Q_y ~=~ | dQ/dy |\, \Delta y } holding all variables but \cc{ y } constant, \emph{etc}.
	The quantities \cc{ \delta Q_x }, \cc{ \delta Q_y }, \dots are called \textsl{partial uncertainties}
	since each only characterizes the uncertainty in \cc{ Q } due to the uncertainty of \emph{one}
	of the variables on which \cc{ Q } depends.
	The \cc{ dQ / dx } term is often written as \cc{ \delta Q / \delta x },
	where this stresses the fact that it is a \textsl{partial derivative} ---
	\emph{i.e.} all other quantities except for \cc{ x } are held constant

\item
	Find \cc{ \Delta Q } by taking the square root of the sum of squares of the partial uncertainties:
\ccc
\beq
	\Delta Q	~~=~~	\sqrt{ \big(\,\delta Q_x \,\big)^2 ~+~ \big(\,\delta Q_y \,\big)^2 ~+~ \big(\,\delta Q_z \,\big)^2 ~+~ \dots~ }
\eeq
\ccb

\item
	Plugging in the definitions of the quantities \cc{ \delta Q_x }, \cc{ \delta Q_y }, \dots, we find that
\ccc
\beq
\label{propagation}
	\Delta Q	~~=~~	\sqrt{ \lgr \left|\, \frac{ dQ }{ dx } \,\right|\, \Delta x \rgr^2 ~+~
					\lgr \left|\, \frac{ dQ }{ dy } \,\right|\, \Delta y \rgr^2 ~+~ \lgr \left|\, \frac{ dQ }{ dz } \,\right|\, \Delta z \rgr^2
					~+~ \dots~ }
\eeq
\ccb
	This formula is known as the \textsl{rule for propagation of uncertainty}.
	Below are two special cases of the error propagation that you will find \text{very} useful in the subsequent laboratories

\end{enumerate}

\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	Suppose \cc{ Q(x,~ y) ~=~ a\,x ~+~ b\, y }, where \cc{ a } and \cc{ b } are constants with no uncertainties.
	Show that the uncertainty in \cc{ Q } due to the uncertainties in \cc{ x } and \cc{ y } is given by
\ccc
\beq
	\Delta Q	~~=~~	\sqrt{ a^2\, \big(\, \Delta x \,\big)^2 ~+~ b^2\, \big(\, \Delta y \,\big)^2 }
	\ccb\,.
	\ccc
\eeq
\ccb
\vspace{4cm}
\end{tcolorbox}

\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	Suppose \cc{ Q(x,~ y) ~=~ k\, x^\alpha\, y^\beta }, where \cc{ k }, \cc{ \alpha } and \cc{ \beta } are constants with no uncertainties.
	Show that the relative uncertainty in \cc{ Q } due to the relative uncertainties in \cc{ x } and \cc{ y } is given by
\ccc
\beq
\label{powers}
	\frac{ \Delta Q }{ |Q| } 	~~=~~	\sqrt{ \alpha^2 \lgr \frac{\Delta x}{x} \rgr^2 ~+~ \beta^2 \lgr \frac{\Delta y}{y} \rgr^2 }
	\ccb\,.
	\ccc
\eeq
\ccb
\vspace{4cm}
\end{tcolorbox}

\begin{tcolorbox}[colframe = deepskyblue, colback = pastelgreen!30,
			title = {\centering Question}]
	Consider an object released from rest near the Earth's surface.
	You know that at time \cc{ t }, the distance \cc{ x } the object has fallen since its release is given by \cc{ x ~=~ \frac 1 2 \, g\, t^2 }

\bigskip\noindent
	Solve this for \cc{ g }.
	Assume that when doing an actual experiment you found that \cc{ t ~=~ \SI{1.2}\s }, \cc{ x ~=~ \SI{7.2}\m }, \cc{ \Delta t ~=~ \SI{0.2}\s }
	and \cc{ \Delta x ~=~ \SI{0.3}\m }.
	Calculate your value for \cc{ g }

\bigskip\noindent
	Use the propagation of uncertainty formula \eqref{propagation} to find \cc{ g } and \cc{ \Delta g }.
	Compare your result to the ``special case'' formula \eqref{powers} that you proved above.
	Are your answers the same?
	Express your final value of \cc{ g } as \cc{ g ~\pm \Delta g }
\vspace{4cm}
\end{tcolorbox}



\end{document}
